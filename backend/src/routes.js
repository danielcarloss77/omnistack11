const express = require("express");
const { celebrate, Segments, Joi } = require("celebrate");

const OngControllers = require("./controllers/OngControllers");
const IncidentsControllers = require("./controllers/IncidentsControllers");
const ProfileController = require("./controllers/ProfileController");
const SessionController = require("./controllers/SessionController");

const routes = express.Router();

routes.post("/sessions", SessionController.create);

routes.get("/ongs", OngControllers.index);
//validando informacao
routes.post(
  "/ongs",
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string()
        .required()
        .email(),
      whatsapp: Joi.number()
        .required()
        .min(10)
        .max(11),
      city: Joi.string().required(),
      uf: Joi.string()
        .required()
        .length(2)
    })
  }),
  OngControllers.create
); //com validacao
//routes.post("/ongs", OngControllers.create); //sem validacao

//com validacao
routes.get(
  "/profile",
  celebrate({
    [Segments.HEADERS]: Joi.object().keys({
      authorization: Joi.string().required()
    })
  }),
  ProfileController.index
);

//routes.get("/profile", ProfileController.index); //sem validacao

routes.get("/incidents", IncidentsControllers.index);
routes.post("/incidents", IncidentsControllers.create);
routes.delete("/incidents/:id", IncidentsControllers.delete);

module.exports = routes;
